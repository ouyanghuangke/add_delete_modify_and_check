﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“testDataSet1.students”中。您可以根据需要移动或删除它。
            this.studentsTableAdapter.Fill(this.testDataSet1.students);
            var abc = "select * from students";

            var dt = DbHelper.GetDataTable(abc);

            dataGridView1.DataSource = dt;

            //限制dataGridView1的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }
        //查询
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from students where Name like '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql );

            dataGridView1.DataSource = dt;
        }

        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm from = new EditForm();
            var res = from.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var abc = "select * from students";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else 
            {
                MessageBox.Show("no");
            }
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["name"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["score"].Value;

            EditForm from = new EditForm(id  ,name ,age ,score);
            var res = from.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var abc = "select * from students";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else 
            {
                MessageBox.Show("no");
            }
        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
                var sql = string.Format("delect from students where id={0}",id);
                var res = DbHelper.AddOrUpdate(sql);

                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var stusql = "select * from students";
                    var dt = DbHelper.GetDataTable(stusql);
                    dataGridView1.DataSource = dt;

                }
                else 
                {
                    MessageBox.Show("删除失败");
                }
            }
            else 
            {
                MessageBox.Show("当前未选择需要删除的数据","提示");
            }
        }
    }
}
