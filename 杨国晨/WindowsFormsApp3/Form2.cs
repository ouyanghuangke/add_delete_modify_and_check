﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        private int Id;
        
        public Form2(int Id,string Name,int Age,string Sex)
        {
            InitializeComponent();
            this.Id = Id;
            textBox1.Text = Name;
            textBox2.Text = Age.ToString();
            textBox3.Text = Sex;
           
        }

        private void Form2_Load(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var Name = textBox1.Text;
            var Age = textBox2.Text;
            var Sex = textBox3.Text;
            if (this.Id>0)
            {
                var sql = string.Format("update student set Name ='{0}',Age={1},Sex='{2}'where Id={3}", Name, Age, Sex,this.Id);
                DBhelper.AddOrUpdateOrDelect(sql);
                MessageBox.Show("更新成功！","提示");

            }
            //添加
            else
            {
                var sql = string.Format("insert into student (Name,Age,Sex) values ('{0}',{1},'{2}')",Name,Age,Sex);
                DBhelper.AddOrUpdateOrDelect(sql);
                MessageBox.Show("添加成功","提示");
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
