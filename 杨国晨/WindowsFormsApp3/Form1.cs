﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolDataSet.student”中。您可以根据需要移动或删除它。
            var s = "select * from student";
            var da = DBhelper.GetDataTable(s);

            dataGridView1.DataSource = da;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from student where Name like '%{0}%'",name);
            var da = DBhelper.GetDataTable(sql);
            dataGridView1.DataSource = da;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            var res =form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from student";
                var da = DBhelper.GetDataTable(sql);

                dataGridView1.DataSource = da;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {


            var id =(int) dataGridView1.SelectedRows[0].Cells[0].Value;
            var Name =(string) dataGridView1.SelectedRows[0].Cells[1].Value;
            var Age= (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var Sex =(string) dataGridView1.SelectedRows[0].Cells[3].Value;
            Form2 form = new Form2(id,Name,Age,Sex);
            var res =form.ShowDialog();
            if (res==DialogResult.Yes)
            {
                var sql = "select * from student" ;
                var da = DBhelper.GetDataTable(sql);
                dataGridView1.DataSource = da;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            var s = dataGridView1.SelectedRows;
            if (s.Count>0)
            {
                var Id = (int)s[0].Cells[0].Value;
                var sql = string.Format("delete from student where Id={0}", Id);

                var sCount = DBhelper.AddOrUpdateOrDelect(sql);

                if (sCount > 0)
                {
                    MessageBox.Show("删除成功!", "提示");
                    var strSql = "select * from student";
                    var da = DBhelper.GetDataTable(strSql);
                    dataGridView1.DataSource = da;
                
                }
                else
                {
                    MessageBox.Show("删除失败！","提示");
                }
            }
            else
            {
                MessageBox.Show("请选择你要删除的行","提示");
            
            }

        }
    }
}
