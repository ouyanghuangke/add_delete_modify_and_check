use master

GO

IF EXISTS(SELECT * FROM SysDatabases WHERE Name ='school')
DROP DATABASE school

GO

CREATE DATABASE school
ON PRIMARY
(
   NAME ='school',
   FILENAME='D:\ѧϰ\school.mdf',
   SIZE=5MB,
   FILEGROWTH=1MB

)
LOG ON
(
   NAME='school_log',
   FILENAME='D:\ѧϰschool_log.ldf',
   SIZE=1MB,
   FILEGROWTH=10%

)
GO
use school
go
IF EXISTS(SELECT * FROM SysObjects where name ='student')
DROP TABLE student
CREATE TABLE student
(
  Id int primary key identity(1,1),
  Name varchar(20) not null,
  Age int not null,
  Sex char(2) not null

)
go

insert into student (Name,Age,Sex) values ('�����',20,'��')
select * from student