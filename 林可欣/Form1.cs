﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //var user = "select * from userInfo";

            var Stu = "select * from Students";

            var dt = DBHelper.GetDataTable(Stu);

            dataGridView1.DataSource = dt;

            //限制dataGridView某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;  //单行选择
            dataGridView1.ReadOnly = true;   //只读
            dataGridView1.AllowUserToAddRows = false;  //没有新行
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text.Trim();
            var sql = string.Format("select * from Students where StudentName like '%{0}%'",name);

            var dt = DBHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm ef = new EditForm();
            var res = ef.ShowDialog();
            if (res==DialogResult.Yes)
            {
                var Stu = "select * from Students";
                 
                var dt = DBHelper.GetDataTable(Stu);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
                var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
                EditForm ef = new EditForm(id, name, age, score);
                var res = ef.ShowDialog();

                if (res == DialogResult.Yes)
                {
                    var Stu = "select * from Students";

                    var dt = DBHelper.GetDataTable(Stu);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No");
                }
            }
            else
            {
                MessageBox.Show("当前未选择要更新的数据", "消息提示框");
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 )
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

                var sql = string.Format("delete from Students where Id = {0}",id);

                var res = DBHelper.AddOrUpdateOrDelect(sql);

            if (res==1)
            {
                MessageBox.Show("删除成功！","消息提示框");

                var Stu = "select * from Students";

                var dt = DBHelper.GetDataTable(Stu);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除失败！", "消息提示框");
            }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "消息提示框");
            } 
        }


    }
}
