﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinformTest_Com
{
     public class Class1
    {
       private static  string conString = "server=.;database=school;uid=sa;pwd=8812345;";

        public static DataTable GetDataTable(string sql)
        {

            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);

            connection.Open();

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            return dt;

        }

        public static int AddOrUpdata(string sql)
        {

            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql, connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }
    }
}
