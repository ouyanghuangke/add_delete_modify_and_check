﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTest_Com
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var a = "select * from employee";

            var dt = Class1.GetDataTable(a);

            dataGridView1.DataSource = dt;

            //限制dataGridView1某些属性
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //使不可编辑
            dataGridView1.ReadOnly = true;
            //没有新行产生
            dataGridView1.AllowUserToAddRows = false;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format(" select * from employee where employeename like '%{0}%'", name);

            var dt = Class1.GetDataTable(sql);

            dataGridView1.DataSource = dt;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();

            var res = form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                MessageBox.Show("Yes");
            }
            else
            {
                MessageBox.Show("No");
            }

            //form.Show();

        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["employeeid"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["employeename"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["employeeage"].Value;
            var native = (string)dataGridView1.SelectedRows[0].Cells["employeenative"].Value;
            Form2 form = new Form2(id, name, age, native);
            var res = form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var a = "select * from employee";

                var dt = Class1.GetDataTable(a);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }

            // form.Show();


        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {

                // var row = dataGridView1.SelectedRows[0];

                // var cell = row.Cells[0];

                var id = (int)dataGridView1.SelectedRows[0].Cells["employeeid"].Value;

                var sql = string.Format(" delete from  employee where id='{0}", id);

                var res = Class1.AddOrUpdata(sql);


                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");

                    var a = "select * from employee";

                    var dt = Class1.GetDataTable(a);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选中需要删除的数据");
            }
        }

    }
}
