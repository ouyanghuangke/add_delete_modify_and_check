﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolDataSet.student”中。您可以根据需要移动或删除它。
            
            var stuSql = "select * from student";

            var dt = DbHelper.GetDataTable(stuSql);

            dataGridView1.DataSource = dt;
            //限制datagridview的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //锁定信息编辑
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format("select * from student where StuName like '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            
        }
        //添加
        private void button3_Click(object sender, EventArgs e)
        {
            EdiForm ediForm = new EdiForm();
            var res =ediForm.ShowDialog();
            if (res==DialogResult.Yes)
            {
                var sql = "select * from student";
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }
        }
        //更新
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];
            var id = (int)row.Cells["Id"].Value;
            var name = (string)row.Cells["StuName"].Value;
            var age = (int)row.Cells["Age"].Value;
            var score = (int)row.Cells["Score"].Value;
            EdiForm ediForm = new EdiForm(id,name,age,score);
            var res =ediForm.ShowDialog();

            if (res == DialogResult.Yes)
            {

                var stuSql = "select * from student";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var id = (int)row.Cells["Id"].Value;

            var sql = string.Format("delete from student where Id={0}",id);

            var res = DbHelper.AddOrUpdateOrDelete(sql);
            if (res == 1)
            {
                MessageBox.Show("删除成功！","提示");
                var stuSql = "select * from student";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除成功！", "提示");
            }
        }
    }
}
