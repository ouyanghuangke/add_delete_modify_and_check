﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private int id;

        public Form2(int id,string name,string pwd,int age,string sex)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = pwd;
            textBox3.Text = age.ToString();
            textBox4.Text = sex;

            
        }
        //取消添加键
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //确认添加键
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var pwd = textBox2.Text;
            var age = textBox3.Text;
            var sex = textBox4.Text;

            //更新
            if (this.id > 0)
            {
                var sql = string.Format("update Userinfo set name = '{0}',pwd = '{1}',age = {2} ,sex = '{3}' where id = {4}", name, pwd, age, sex, this.id);
                DBHelper.Add(sql);
                MessageBox.Show("更新成功！","提示");
            }
            //添加
            else
            {
                var sql = string.Format("insert into Userinfo (name,pwd,age,sex) values ('{0}','{1}',{2},'{3}')", name, pwd, age, sex);
                DBHelper.Add(sql);
                MessageBox.Show("添加成功！","提示");
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
