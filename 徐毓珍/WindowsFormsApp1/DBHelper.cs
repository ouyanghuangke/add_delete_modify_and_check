﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DBHelper
    {
        //连接数据库
        private static string connStr = "server=.;database=Users;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            //声明数据库对象
            SqlConnection conn = new SqlConnection(connStr);

            //new一个SqlDataAdapter指令对象连接sql和conn
            SqlDataAdapter sda = new SqlDataAdapter(sql, conn);

            //打开连接
            conn.Open();

            //new一个DataTable获取数据
            DataTable dataTable = new DataTable();
            sda.Fill(dataTable);
            return dataTable;
        }
        public static int Add(string sql)
        {
            //声明数据库对象
            SqlConnection conn = new SqlConnection(connStr);

            //new一个sqlcommandz指令对象连接sql和conn
            SqlCommand cmd = new SqlCommand(sql, conn);

            //打开连接
            conn.Open();

            return cmd.ExecuteNonQuery();
        }
    }
}
