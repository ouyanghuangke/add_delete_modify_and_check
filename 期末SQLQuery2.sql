--创建students数据库
CREATE DATABASE Students 
 ON PRIMARY 
 (
 NAME='Students_data',
 FILENAME='D:\我的学习\Students_data.mdf',
 SIZE=5MB,
 FILEGROWTH=10%
 )
 LOG ON
  (
  NAME='Students_log',
 FILENAME='D:\我的学习\Students_log.ldf',
 SIZE=50MB,
 FILEGROWTH=1MB
  )
  GO
  
  --创建表
  USE Students 
   GO 
   CREATE TABLE StuInfo
   (
   StuID int NOT NULL,
   StuName char(20) NOT NULL,
   StuSex bit NOT NULL,
   StuScore int NOT NULL, 
   )
   GO

   --增加表格中的数据
   INSERT INTO StuInfo (StuID,StuName,StuSex,StuScore)  VALUES (19001,'王月馨','女',86)
   INSERT INTO StuInfo (StuID,StuName,StuSex,StuScore)  VALUES (19002,'全贵兰','女',84)
   INSERT INTO StuInfo (StuID,StuName,StuSex,StuScore)  VALUES (19003,'王子晴','女',85)
   INSERT INTO StuInfo (StuID,StuName,StuSex,StuScore)  VALUES (19004,'夏雪若','女',80)
   INSERT INTO StuInfo (StuID,StuName,StuSex,StuScore)  VALUES (19005,'小明','男',75)
   INSERT StuInfo (StuID,StuName,StuSex,StuScore)
   SELECT 19006,'谢丹','女',90
   SELECT 19007,'王青','男',59

   --删除表中多余数据
    DELETE FROM StuInfo   where StuID='19006'
	--查看表中数据
   select * from StuInfo 